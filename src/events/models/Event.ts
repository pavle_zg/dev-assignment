import mongoose from "mongoose";

const EventSchema = new mongoose.Schema({
    name: String,
    about: String,
    short: String,
    logo: String,
    time: {
        from: Date,
        to: Date
    },
    location: {
        country: String,
        countryCode: String,
        address: String,
        city: String,
        latitude: Number,
        longitude: Number
    }
}, { timestamps: true });
const Event = mongoose.model("Event", EventSchema);
export default Event;