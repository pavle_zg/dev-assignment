import { BaseRouter } from '../common/BaseRouter';
import { EventsController } from './EventsController';

class EventsRouter extends BaseRouter {

    private controller: EventsController;

    constructor() {
        super();
        this.controller = new EventsController();
    }

    registerRoutes(): void {
        this.router.get("/event_info", this.async(this.event()));
    }

    event() {
        return async (req: any, res: any, next: any) => {
            const event = await this.controller.loadEvent();
            res.status(200).json({
                message: "OK",
                event
            })
        }
    }
}

const eventsRouter = new EventsRouter();
export default eventsRouter;