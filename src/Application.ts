import express from "express";
import bodyParser from "body-parser";
import http from "http";
import config from "./common/config";
import router from "./common/router";
import mongoose from "mongoose";

const app = express();
const server = new http.Server(app);

export class Application {

    constructor() {
        app.use((req, res, next) => {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Methods", "GET,POST,OPTIONS");
            res.header("Access-Control-Allow-Headers", "Origin,Content-Type, Cache-Control, X-Requested-With, Accept, Content-Length, x-access-token, channel, event");
            res.header("Access-Control-Expose-Headers", "x-access-token");
            if ("OPTIONS" === req.method) {
                res.status(200).send({ message: "OK" });
            } else {
                next();
            }
        });
        app.use(bodyParser.json());
        app.use(bodyParser.text({ limit: "5000kb" }));
        app.use("", router);

        app.use((req, res, next) => {
            if (req.method === "POST" && !req.body) {
                res.status(400).json("Bad Request")
            } else {
                next()
            }
        });
        //if requested route is not found return 404 response
        app.use((req, res, next) => {
        });
        //if no route is found or error occured log error and return 404 or 500 response
        app.use((err: any, req: any, res: any, next: any) => {
        });
    }

    start() {
        server.listen(config.port, () => {
            console.log(`    Server listening on port ${config.port}`);
        });
        // FIXME: this is not the best way to define a constant. why? should we move it somewhere else?
        mongoose.connect("mongodb://fsd-assignment:j9uFT2ViZBzYmQLVwdirLfej5dUxYgiOorvZujGjTjJYfveeWEviu2yvhNPukvLeHjhrRPRDqm6g3qp9QgBCQw%3D%3D@fsd-assignment.documents.azure.com:10255/?ssl=true")
            .then(() => console.log("DB connected"))
            .catch(error => console.log(error));
    }
}