# Full-stack developer assignment. 

## Intro
Assignment is divided to backend and frontend part. Frontend brings you 70% and backend 30% of success. This is the real-life challenge we meet here at Eventrify every day.
You can see our demo registration form https://test.get2.events/demo/register.

How does it work from the high level perspective? 
Our client creates an event with certain properties, so end users could register to that event online. Our client can keep on track with how many users already registered, who payed etc.

What matters to you the most is - how does it work from technical perspective?
When form (React app) loads, it fetches Event/Congress data from our backend:
 (https://endpoint.com/event_info), 
in JSON format similar to this:
```
{
    "name": "Eventrify Congress",
    "about": "Something about us that goes in one line",
    "short": "eve2018",
    "logo": "http://eventrify.us/images/levw.png",
    "time": {
        "from" : "2018-10-08T08:00:00.104Z",
        "to" : "2018-10-11T18:00:00.104Z"
    },
    "location": {
        "country" : "Croatia",
        "countryCode" : "HR",
        "address" : "Selska cesta",
        "city" : "Zagreb",
        "latitude" : 45.802332,
        "longitude" : 15.9429029
    }
}
```

Then, populate the views with the given data. After the end-user choose his preferences, by clicking on payment button, a form (React app) sends specific JSON data to the backend to process the transaction.

## Your assignment:

### Notes
Read comments in backend code. TODO's and FIXME's are micro assignments.

### Frontend
Create new React app from scratch using `createReactApp` and try to recreate our registration form (not entirely of course). The main focus of your assignment is creating the component for entering credit card information and validation. Similar to what you can see on our form. Next focus is header aesthetic and inputs for user data. We don't expect you to recreate whole registration form and all the cool animations. No pressure. Do as much as you can. You know what focus is, so stay focused :) but! Bare in mind that registration form that has credit card info and no event info and user inputs - is not really a registration form...

### Backend
Second (or first) part of an assignment is a backend part. We wrote you a small node server to serve your site. You need it to provide you the data for your home page/registration form (Event/Congress data). You need to expose needed routes using `express.js` and `mongoose.js` to load data from Mongo DB. This route should look like `GET /events/event_info` and returns an Event collection in JSON representation. You can find event router on path `src/events/EventsRouter`. Implement logic in `EventsController` class. On the frontend side, to cennect to server's API, you can use `axios` or any other http client you prefer. Recommended express middleware for JSON parsing is `body-parser`. After your backend is ready, start the server locally and start serving your frontend.

### Conclusion
Backend code is hosted on a git repository on Bitbucket https://bitbucket.org/pavle_zg/dev-assignment/src. You need to fork it (or just checkout it) and use it on your own git client. Please don't try to push anything in it.
When you are done, create two new repositories. One should host backend code and the other frontend code. At the end, just reply to my email with links to git repositories.